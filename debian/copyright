Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pycifrw
Source: https://github.com/jamesrhester/pycifrw

Files: *
Copyright: James Hester <jamesrhester@gmail.com>
License: Python-2.0

Files: docs/markdown4.css
Copyright: 2012, Florian Wolters
License: LGPL-3+
 On Debian systems the full text of the LGPL-3 can be found in
 /usr/share/common-licenses/LGPL-3

Files: src/yapps3/*
 src/yapps3_compiled_rt.py
Copyright: 1999-2003, Amit J. Patel <amitp@cs.stanford.edu>
License: Expat

Files: tests/C13H2203_with_errors.cif
 tests/C13H22O3.cif
Copyright: International Union of Crystallography
License: public-domain
 The IUCr policy for the protection and the promotion of the STAR File and CIF
 standards for exchanging and archiving electronic data
 .
 Overview
 .
 The Crystallographic Information File (CIF)[1] is a standard for information
 interchange promulgated by the International Union of Crystallography (IUCr).
 CIF (Hall, Allen & Brown, 1991) is the recommended method for submitting
 publications to Acta Crystallographica Section C and reports of crystal
 structure determinations to other sections of Acta Crystallographica and many
 other journals. The syntax of a CIF is a subset of the more general STAR
 File[2] format. The CIF and STAR File approaches are used increasingly in the
 structural sciences for data exchange and archiving, and are having a
 significant influence on these activities in other fields.
 .
 Statement of intent
 .
 The IUCr's interest in the STAR File is as a general data interchange standard
 for science, and its interest in the CIF, a conformant derivative of the STAR
 File, is as a concise data exchange and archival standard for crystallography
 and structural science.
 .
 Protection of the standards
 .
 To protect the STAR File and the CIF as standards for interchanging and
 archiving electronic data, the IUCr, on behalf of the scientific community,
 .
  * holds the copyrights on the standards themselves,
  * owns the associated trademarks and service marks, and
  * holds a patent on the STAR File.
 .
 These intellectual property rights relate solely to the interchange formats,
 not to the data contained therein, nor to the software used in the generation,
 access or manipulation of the data.
 .
 Promotion of the standards
 .
 The sole requirement that the IUCr, in its protective role, imposes on software
 purporting to process STAR File or CIF data is that the following conditions be
 met prior to sale or distribution.
 .
  * Software claiming to read files written to either the STAR File or the CIF
    standard must be able to extract the pertinent data from a file conformant
    to the STAR File syntax, or the CIF syntax, respectively.
  * Software claiming to write files in either the STAR File, or the CIF,
    standard must produce files that are conformant to the STAR File syntax, or
    the CIF syntax, respectively.
  * Software claiming to read definitions from a specific data dictionary
    approved by the IUCr must be able to extract any pertinent definition which
    is conformant to the dictionary definition language (DDL)[3] associated with
    that dictionary.
 .
 The IUCr, through its Committee on CIF Standards, will assist any developer to
 verify that software meets these conformance conditions.
 .
 Glossary of terms
 .
  * [1] CIF: is a data file conformant to the file syntax defined at
    http://www.iucr.org/iucr-top/cif/spec/index.html
  * [2] STAR File: is a data file conformant to the file syntax defined at
    http://www.iucr.org/iucr-top/cif/spec/star/index.html
  * [3] DDL: is a language used in a data dictionary to define data items in
    terms of "attributes". Dictionaries currently approved by the IUCr, and the
    DDL versions used to construct these dictionaries, are listed at
    http://www.iucr.org/iucr-top/cif/spec/ddl/index.html

Files: debian/*
Copyright: 2019-2023, Andrius Merkys <merkys@debian.org>
License: Expat

License: Python-2.0
 PYCIFRW License Agreement (Python License, Version 2)
 -----------------------------------------------------
 .
 1. This LICENSE AGREEMENT is between the Australian Nuclear Science
 and Technology Organisation ("ANSTO"), and the Individual or
 Organization ("Licensee") accessing and otherwise using this software
 ("PyCIFRW") in source or binary form and its associated documentation.
 .
 2. Subject to the terms and conditions of this License Agreement,
 ANSTO hereby grants Licensee a nonexclusive, royalty-free, world-wide
 license to reproduce, analyze, test, perform and/or display publicly,
 prepare derivative works, distribute, and otherwise use PyCIFRW alone
 or in any derivative version, provided, however, that this License
 Agreement and ANSTO's notice of copyright, i.e., "Copyright (c)
 2001-2014 ANSTO; All Rights Reserved" are retained in PyCIFRW alone or
 in any derivative version prepared by Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on
 or incorporates PyCIFRW or any part thereof, and wants to make the
 derivative work available to others as provided herein, then Licensee
 hereby agrees to include in any such work a brief summary of the
 changes made to PyCIFRW.
 .
 4. ANSTO is making PyCIFRW available to Licensee on an "AS IS"
 basis. ANSTO MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED. BY WAY OF EXAMPLE, BUT NOT LIMITATION, ANSTO MAKES NO AND
 DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYCIFRW WILL NOT
 INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. ANSTO SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYCIFRW
 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS A
 RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYCIFRW, OR ANY
 DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material
 breach of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between ANSTO
 and Licensee. This License Agreement does not grant permission to use
 ANSTO trademarks or trade name in a trademark sense to endorse or
 promote products or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using PyCIFRW, Licensee agrees
 to be bound by the terms and conditions of this License Agreement.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
